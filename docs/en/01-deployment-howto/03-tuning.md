# Basic tuning settings

>**DISCLAIMER**: this is not legal nor technical advice. It is just a WIP proposal, open to discussion, on how a Jitsi server can be quickly (and legally) deployed in order to host one's own private videomeetings, based on authors' practical experiences.

Jitsi default settings may work well with conferences with only a few participants, but as the number of participants increases, some of them may experience problems and crashes (not due to Jitsi server but to bandwidth and hardware limitations on the client side): by default each client encode (and send to the server) multiple video stream layers and, at the same time, receive everyone else's at an ideal resolution of 720p.

## Change default language

You may want to change default interface language, if you are located in a non-English speaking country.  
Users may still choose they preferred language in client settings.

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

edit language code `defaultLanguage: 'en'`

## Force users to set a display name

You may want to force users to choose a display name, to easily identify who is talking.

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

set `requireDisplayName: true` (and uncomment if necessary)

<!-- ## Set H264 codec

You may want to set H264 codec instead of VP8, to save up resources on clients.

set `preferH264: true` (and uncomment if necessary) -->

## Enable Layer Suspension

You may want to suspend unused video layers until they are requested again, to save up resources on both server and clients.

set `enableLayerSuspension: true` (and uncomment if necessary)

read more on this feature [here](https://jitsi.org/news/new-off-stage-layer-suppression-feature/)

## Limit video resolution

You may want to limit the maximum video resolution, to save up resources on both server and clients.

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

for example: set `resolution: 480` (and uncomment if necessary);
uncomment the entire `constraints` section and set `ideal: 480`, `max: 480`

## Limit number of video feeds forwarded

You may want to limit the number of video feeds forwarded to each client, to save up resources on both server and clients.
As clients' bandwidth and CPU may not bear the load, use this setting to avoid lag and crashes.  
This feature is found by default in other webconference applications such as Office 365 Teams (limit is set to 4).  
Read how it works on [Jitsi official doc] and performance evaluation on [this study].

[Jitsi official doc]: https://github.com/jitsi/jitsi-videobridge/blob/master/doc/last-n.md
[this study]: https://jitsi.org/wp-content/uploads/2016/12/nossdav2015lastn.pdf

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

set number of unmuted video feeds `channelLastN: 4` (and uncomment if necessary)

## Deactivate webcams at start by default

You may want to start the conference with all video feeds deactivated, to save up resources both on server and clients.

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

set `startAudioOnly: true` (and uncomment if necessary)

## Deactivate webcams after the Nth participant at start

You may want to start the conference with a few video feeds, to keep order.

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

set number of unmuted webcams `startVideoMuted: 10` (and uncomment if necessary)

## Deactivate mics at start by default

You may want to start the conference with all audio feeds deactivated, to keep order.

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

## Deactivate mics after the Nth participant at start

You may want to start the conference with a few audio feeds, to keep order.

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

set number of unmuted mics `startAudioMuted: 10` (and uncomment if necessary)
