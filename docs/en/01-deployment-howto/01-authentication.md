# Enable authentication

>**DISCLAIMER**: this is not legal nor technical advice. It is just a WIP proposal, open to discussion, on how a Jitsi server can be quickly (and legally) deployed in order to host one's own private videomeetings, based on authors' practical experiences.

## Proposed Jitsi implementation

The proposed Jitsi implementation requires authentication for the host, but not for guests.  
More precisely, while hosts need to have an account to create rooms, guests do not need to have one, but should be required to provide a room-specific password to enter. Thus to minimize privacy and security issues while keeping the system practical and easy to use for everybody.

## The problem solved here, not (yet) covered by official documentation 

As already said in platform introduction, user authentication may be added in various ways; authentication control belongs into the XMPP server on which Jitsi relies/depends ([Prosody]). [Prosody] has a simple built-in authentication system (with passwords stored in plain text or hashed, depending on the module used); many other authentication services can be implemented through dedicated [Prosody] modules.  

In Jitsi official documentation, the suggested way to set user passwords is using `prosodyctl` command, that can only be run by the sysadmin. Unfortunately, **this is *not* GDPR-compliant**, because "enabling users to set their password without the admin knowing it" is a basic and unavoidable security measure. On the other hand, configuring Prosody to allow users to register independently their own account is not even an option, as anyone out there would be able to use your Jisti.

Since Prosody server is accessible independently from Jisti-meet, the solution is ask users to change their password through an external [XMPP client], like [Pidgin]. In this way, basic GDPR compliance can be achieved.

[XMPP client]: <https://xmpp.org/software/clients.html>
[Pidgin]: <https://pidgin.im/>

<!-- FIXME: cosa intendevi per "or the like -- or, if an external application is used for authentication, they can change it though such application -->

<!-- TODO: check prosody module mod_auth_web as a possible alternative --- still beta, though! -->

The only issue is that **in the default Jitsi deployment** (quick install) **prosody uses self-signed certificates**, so XMPP clients may display error messages (or even refuse to connect). Therefore **some extra manual configuration is needed, that is covered by this howto**.

## Configure authentication only for room creation

taken from [Jitsi-Jicofo official documentation](https://github.com/jitsi/jicofo/blob/master/README.md#secure-domain)

change prosody config:
```shell
sudo nano /etc/prosody/conf.avail/$(hostname -f).cfg.lua
```

change authentication from `anonymous` to `internal_hashed`, and 
add a second VirtualHost after the first VirtualHost section (use your actual domain):

```lua
VirtualHost "guest.meet.example.com"
    authentication = "anonymous"
    c2s_require_encryption = false
```

save, exit, then:

```shell
sudo nano /etc/jitsi/meet/$(hostname -f)-config.js
```

after the line `domain: 'meet.example.com',` insert another line `anonymousdomain: 'guest.meet.example.com',` (use your actual domain and don't forget the commas!); 

save, exit, then:

```shell
sudo nano  /etc/jitsi/jicofo/sip-communicator.properties
```

put the following line (rember to insert your actual domain):

```
org.jitsi.jicofo.auth.URL=XMPP:meet.example.com
```

save and exit.

## Certificates for prosody

Add the following temporary configuration to nginx, just to be able to generate valid letsencrypt certificates for `auth.meet.example.com` subdomain, used by prosody (as an alternative, you could use the standalone option of certbot, but this would imply to stop nginx server - and Jitsi - for a while).

```shell
sudo nano /etc/nginx/sites-available/auth.$(hostname -f).conf
```

paste the following text (remember to insert your actual domain):

```conf
server {
   listen 80;
   server_name auth.meet.example.com;
   root /usr/share/jitsi-meet;
   index index.html index.htm;
   error_page 404 /static/404.html;
}
```

save and exit.  
Then install letsencrypt certificate for your `auth.meet.example.com` domain:

```shell
sudo ln -s /etc/nginx/sites-available/auth.$(hostname -f).conf /etc/nginx/sites-enabled/
sudo service nginx reload
sudo certbot-auto certonly --noninteractive --webroot --webroot-path /usr/share/jitsi-meet \
-d auth.$(hostname -f) --agree-tos --email PUT_YOUR_EMAIL_ADDRESS_HERE
```

wait for the process to successfully complete (otherwise relaunch the last command above).  
Then remove temporary nginx configuration:

```shell
sudo rm /etc/nginx/sites-enabled/auth.$(hostname -f).conf
sudo service nginx reload
```

Import all letsencrypt certificates in prosody (ignore warnings for guest, conference, localhost, jitsi-videobridge and focus hosts) and reboot:

```shell
sudo prosodyctl --root cert import /etc/letsencrypt/live
sudo reboot
```

## Certificates renewal

In order to enable automatic certificates renewal, pre and post hook scripts have to be added.

```shell
sudo nano /etc/letsencrypt/renewal-hooks/pre/00-prosody-auth.sh
```

paste the following text:

```bash
#!/bin/bash
FQDN=$(hostname -f)
ln -s /etc/nginx/sites-available/auth.$FQDN.conf /etc/nginx/sites-enabled/auth.$FQDN.conf
service nginx reload
```

save and exit.  
Then:

```shell
sudo chmod +x /etc/letsencrypt/renewal-hooks/pre/00-prosody-auth.sh
sudo nano /etc/letsencrypt/renewal-hooks/post/00-prosody-auth.sh
```

paste the following text:

```bash
#!/bin/bash
FQDN=$(hostname -f)
rm /etc/nginx/sites-enabled/auth.$FQDN.conf
service nginx reload
prosodyctl --root cert import /etc/letsencrypt/live
```

save and exit.
Then:

```shell
sudo chmod +x /etc/letsencrypt/renewal-hooks/post/00-prosody-auth.sh
```

you can test it forcing a renewal:

```shell
sudo certbot-auto renew --force-renew

```

which should output something like (ignore warnings for guest, conference, localhost, jitsi-videobridge and focus hosts):

```
Saving debug log to /var/log/letsencrypt/letsencrypt.log

[...]

Congratulations, all renewals succeeded. The following certs have been renewed:
  /etc/letsencrypt/live/auth.meet.example.com/fullchain.pem (success)
  /etc/letsencrypt/live/meet.example.com/fullchain.pem (success)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Running post-hook command: /etc/letsencrypt/renewal-hooks/post/00-prosody-auth.sh
Output from post-hook command 00-prosody-auth.sh:
No certificate for host focus.meet.example.com found :(
No certificate for host localhost found :(
No certificate for host jitsi-videobridge.meet.example.com found :(
No certificate for host conference.meet.example.com found :(
No certificate for host guest.meet.example.com found :(
Imported certificate and key for hosts meet.example.com, auth.meet.example.com
```

Now you should have certificates automatically renewed.  
If there are problems with the renewal, you will be notified by email. In such case, run again the steps described in the [previous section](#certificates-for-prosody).

## Create Moderator Users

After reboot, create users (moderators):

```shell
sudo prosodyctl register USER_NAME_OF_YOUR_CHOICE $(hostname -f) TEMPORARY_PASSWORD
```

To change their password at first use, moderators have to
- log in at `meet.example.com` through [Pidgin] or any other [XMPP client] using the account credentials set above
- change their password through the XMPP client (in pidgin, menu Account->ACCOUNT_NAME->change password).

[pidgin]: <https://pidgin.im/>
[XMPP client]: <https://xmpp.org/software/clients.html>


## Fail2Ban for Prosody

You should now add a new fail2ban filter and jail for prosody. But before you have to install and enable mod_log_auth in prosody.

```shell
sudo apt install -y mercurial
hg clone https://hg.prosody.im/prosody-modules/ prosody-modules
sudo cp prosody-modules/mod_log_auth/mod_log_auth.lua /usr/lib/prosody/modules/
sudo nano /etc/prosody/conf.d/$(hostname -f).cfg.lua
```

add `log_auth` in the enabled modules list/table:

```lua
-- we need bosh
modules_enabled = {
    "bosh";
    "pubsub";
    "ping"; -- Enable mod_ping
    "log_auth";
}
```

restart prosody

```shell
sudo service prosody restart
```

If you want to check if the module works, do:

```shell
sudo tail -f /var/log/prosody/prosody.log
```

and at the same time, with a web browser, try to login at `meet.example.com` by creating a new room and clicking on "I am the host", type a wrong user/password, and see what happens in the log. In terminal, to stop the test press CTRL+C.

Then configure fail2ban (**NOTE**: the configuration guide at <https://modules.prosody.im/mod_log_auth.html> is partially wrong and it does not fit our needs here):

```shell
sudo nano /etc/fail2ban/filter.d/prosody-auth.conf
```

put the following text:

```
# /etc/fail2ban/filter.d/prosody-auth.conf
# Fail2Ban configuration file for prosody authentication
[Definition]
failregex = Failed authentication attempt \(not-authorized\) for user .* from IP: <HOST>
ignoreregex =
```

then 

```shell
sudo nano /etc/fail2ban/jail.local
```

put the following text

```
[prosody]
enabled = true
iptables-multiport[name="prosody", port="443,5222,5269"]
port    = 443,5222,5269
filter  = prosody-auth
logpath = /var/log/prosody/prosody*.log
maxretry = 5
```

(we need to ban also on port 443 because user usually logs in via jitsi web interface, and not only through an XMPP client)

Restart Fail2Ban

```shell
sudo service fail2ban restart
```

You can try if it works by monitoring the log of Fail2Ban:

```shell
sudo tail -f /var/log/fail2ban.log
```

and at the same time, with a web browser, try to login at `meet.example.com` by creating a new room and clicking on "I am the host", type a wrong user/password for more than 6 times, and see what happens in the log. In terminal, to stop the test press CTRL+C.
