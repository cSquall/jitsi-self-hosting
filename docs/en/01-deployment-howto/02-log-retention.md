# Log Retention Policy

>**DISCLAIMER**: this is not legal nor technical advice. It is just a WIP proposal, open to discussion, on how a Jitsi server can be quickly (and legally) deployed in order to host one's own private videomeetings, based on authors' practical experiences.

As already mentioned in the [introduction](../00-introduction/01-platform-description.md#personal-data), **running a Jitsi server *does* involve the processing of personal data** (IP address, username, displayname, browser, OS, pages -- i.e. conference rooms -- visited, login/logout times, etc.). These personal data  are also stored in the logs of various software components -- namely:

- jicofo
- jitsi-videobridge (jvb) 
- nginx
- prosody
- fail2ban
- rsyslog
- ufw

In most use contexts, such log data should be retained only for a short period of time (which may be indicatively set to 7 days, to comply with GDPR "storage limitation" principle), and just for security reasons (which constitute a legitimate interest of the Data Controller, in the sense of the GDPR). 

## logrotate configuration

In order to do so, go to `/etc/logrotate.d` and edit the config files for each of the packages/components above. We suggest to **delete** any `hourly`/`weekly`/`monthly` setting as well as any `rotate` and `size` setting, and **replace** them with the following settings:

```
daily
maxsize 100M
missingok
maxage 7
dateext
```

Particularly:

- `daily` creates daily log files; 

- `maxsize 100M` splits log files when they reach 100MB (while `size` should be avoided because it overrides log retention settings, and retain logs until they reach the set size);

- `missingok`: don’t write an error message if the log file is missing; 

- `maxage 7` deletes log files only if they are older than 7 days (while rotate just counts the number of log files and it could delete them before the 7th day, e.g. when you have more than 1 log in 1 day, as the first was > 100MB)

- `datext`: optional, it adds date tag to log file names, instead of just a number.

After modification, restart logrotate service:

```shell
sudo service logrotate restart
```
