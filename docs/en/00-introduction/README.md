# Introduction

Pursuing **decentralized, quick-to-deploy, user-owned cloud solutions** is a matter of **privacy and resilience**: centralized cloud solutions mean that personal (meta)data and content of private conversations of hundreds/thousands/millions of people are processed by a single entity, which constitutes a huge single point of failure, both from a privacy and from a service availability point of view.

As smart working in the era of COVID-19 is teaching to all of us (with problems of overcrowding, insufficient bandwidth, traffic bottlenecks between clients and centralized servers, etc.), **in case of emergency, decentralized solutions are proven to be more resilient and effective to keep services available** to all (or at least to most). In the end, the Internet itself was originally designed as a fully decentralized network to the very same purpose (to be resilient also in case of war).

## Why Jitsi

Because it is a stable and reliable open source videoconferencing solution that can be deployed in minutes at any time on an inexpensive server (no special horsepower requirements; no need for any particular data storage, configuration, interfacing with external applications etc.). And it is very easy to use on client side, too. 

## Why self-hosted

Someone may say "why can't I use one of the dozens of Jitsi instances already deployed and available for everybody?". You can, but you have to *trust* the host: it would be just like using any commercial alternative. Open source on its own does mean nothing if you cannot look under the hood: for example, the host could activate chat logging or video recording without your knowledge. Besides, a single host serving a lot of third party conversations may suffer from bandwidth/system overload and is more likely to be targeted by malicious hackers.

**REMEMBER** *"There's no cloud... just other people's computers"*
